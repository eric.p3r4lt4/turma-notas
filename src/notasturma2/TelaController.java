/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notasturma2;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;

/**
 *
 * @author Cliente
 */
public class TelaController implements Initializable {
    Turma p = new Turma();
    int c = 0;
    int a=0;
    
    //Label
    @FXML
    private Label pesoLabel;
    @FXML
    private Label nomeCadastr;
    @FXML
    private Label nota1Cadastr;
    @FXML
    private Label nota2Cadastr;
    @FXML
    private Label nota3Cadastr;
    @FXML
    private Label prova1Cadastr;
    @FXML
    private Label prova2Cadastr;
    @FXML
    private Label trabCadastr;
    @FXML
    private Label pronto;
    @FXML
    private Label nomeAlunoLabel;
    @FXML
    private Label histxmedia;
    @FXML
    private Label showTurma;
    
    //TextField
    @FXML
    private TextField pesoText;
    @FXML
    private TextField nomeCadText;
    @FXML
    private TextField nota1Text;
    @FXML
    private TextField nota2Text;
    @FXML
    private TextField nota3Text;
    @FXML
    private TextField p1Text;
    @FXML
    private TextField p2Text;
    @FXML
    private TextField trabText;
    @FXML
    private TextField nomeAlunoText;
    
    //Button
    @FXML
    private Button getPeso;
    @FXML
    private Button cadastrar;
    @FXML
    private Button calcTri;
    @FXML
    private Button histShow;
    @FXML
    private Button turmaShow;
    @FXML
    private Button getAluno;
    @FXML
    private Button OK;
    @FXML
    private Button nomeAlunoGet;
    @FXML
    private Button perronto;
    @FXML
    private Button proximo;
    
    //Métodos
    @FXML
    private void peso() {
        if(pesoLabel.getText().equals("O peso (de um a dez) da nota 1:")){
            p.setPeso(0,Double.parseDouble(pesoText.getText()));
            pesoLabel.setText("O peso (de um a dez) da nota 2:");
            pesoText.setText("");
        }
        
        else if(pesoLabel.getText().equals("O peso (de um a dez) da nota 2:")){
            p.setPeso(1,Double.parseDouble(pesoText.getText()));
            pesoLabel.setText("O peso (de um a dez) da nota 3:");
            pesoText.setText("");
        }
        
        else{
            p.setPeso(2,Double.parseDouble(pesoText.getText()));
            pesoLabel.setVisible(false);
            pesoText.setVisible(false);
            pesoText.setEditable(false);
            getPeso.setVisible(false);
            cadastrar.setVisible(true);
            calcTri.setVisible(true);
            histShow.setVisible(true);
            turmaShow.setVisible(true);
        }
    }
    
    @FXML
    private void cadastrarAluno(){
        bloqueia(true);
        nomeCadastr.setVisible(true);
        nota1Cadastr.setVisible(true);
        nota2Cadastr.setVisible(true);
        nota3Cadastr.setVisible(true);
        prova1Cadastr.setVisible(true);
        prova2Cadastr.setVisible(true);
        trabCadastr.setVisible(true);
        
        nomeCadText.setVisible(true);
        nota1Text.setVisible(true);
        nota2Text.setVisible(true);
        nota3Text.setVisible(true);
        p1Text.setVisible(true);
        p2Text.setVisible(true);
        trabText.setVisible(true);
        
        getAluno.setVisible(true);
    }
    
    @FXML
    private void getAlunoButton(){
        p.setAlunosNome(c, (nomeCadText.getText()));
        
        p.setAlunosHist(c, 0, Double.parseDouble(nota1Text.getText()));
        p.setAlunosHist(c, 1, Double.parseDouble(nota2Text.getText()));
        p.setAlunosHist(c, 2, Double.parseDouble(nota3Text.getText()));
        
        p.setAlunosNotas(c, 0, Double.parseDouble(p1Text.getText()));
        p.setAlunosNotas(c, 1, Double.parseDouble(p2Text.getText()));
        p.setAlunosNotas(c, 2, Double.parseDouble(trabText.getText()));
        
        bloqueia(false);
        
        nomeCadastr.setVisible(false);
        nota1Cadastr.setVisible(false);
        nota2Cadastr.setVisible(false);
        nota3Cadastr.setVisible(false);
        prova1Cadastr.setVisible(false);
        prova2Cadastr.setVisible(false);
        trabCadastr.setVisible(false);
        
        nomeCadText.setVisible(false);
        nomeCadText.setText("");
        nota1Text.setVisible(false);
        nota1Text.setText("");
        nota2Text.setVisible(false);
        nota2Text.setText("");
        nota3Text.setVisible(false);
        nota3Text.setText("");
        p1Text.setVisible(false);
        p1Text.setText("");
        p2Text.setVisible(false);
        p2Text.setText("");
        trabText.setVisible(false);
        trabText.setText("");
        
        getAluno.setVisible(false);
        
        c++;
    }
    
    @FXML
    private void calcTriAluno(){
        bloqueia(true);
        for(int i=0;i<c;i++){
            double m = (p.getNotaAluno(i, 0)*(p.getPeso(0)/10)) + 
                       (p.getNotaAluno(i, 1)*(p.getPeso(1)/10)) +
                       (p.getNotaAluno(i, 2)*(p.getPeso(2)/10));
            p.setMAluno(i,m);
        }
        
        pronto.setVisible(true);
        OK.setVisible(true);
    }
    
    @FXML
    private void OkButton(){
        bloqueia(false);
        pronto.setVisible(false);
        OK.setVisible(false);
    }
    
    @FXML
    private void histShowAluno(){
        bloqueia(true);
        
        nomeAlunoLabel.setVisible(true);
        nomeAlunoText.setVisible(true);
        nomeAlunoGet.setVisible(true);
        nomeAlunoGet.setDisable(false);
    }
    
    @FXML
    private void comparaMediaEHist(){
        String name = nomeAlunoText.getText();
        nomeAlunoText.setText("");
        nomeAlunoGet.setDisable(true);
        
        for(int i=0;i<c;i++){
            if((p.getNomeAluno(i)).equals(name)){
                histxmedia.setText("Prog I (tri 1): " + p.getHistAluno(i,0) +
                                   "\nProg II (media): " + p.getMAluno(i));
            }
        }
        histxmedia.setVisible(true);
        perronto.setVisible(true);
    }
    
    @FXML
    private void sairHistShow(){
        nomeAlunoLabel.setVisible(false);
        nomeAlunoText.setVisible(false);
        nomeAlunoGet.setVisible(false);
        histxmedia.setVisible(false);
        perronto.setVisible(false);
        bloqueia(false);
    }
    
    @FXML
    private void turmaShowAluno(){
        bloqueia(true);
        showTurma.setVisible(true);
        proximo.setVisible(true);
        if(a < (c-1)){
            showTurma.setText(p.getNomeAluno(a)+": "+p.getMAluno(a));
            a++;
        }
        else if(a == (c-1)){
            showTurma.setText(p.getNomeAluno(a)+": "+p.getMAluno(a));
            proximo.setText("Sair");
            a++;
        }
        else if(a == c){
            bloqueia(false);
            showTurma.setVisible(false);
            proximo.setVisible(false);
            proximo.setText("Proximo aluno");
            a=0;
        }
    }
    
    public void bloqueia(boolean f){
        cadastrar.setDisable(f);
        calcTri.setDisable(f);
        histShow.setDisable(f);
        turmaShow.setDisable(f);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}