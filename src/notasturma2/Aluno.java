package notasturma2;

public class Aluno {
    private String nome;
    private double[] hist = new double[3];
    private double[] notas = new double[3];
    private double m;

    public String getNome() {
        return nome;
    }
    
    public double getNotas(int i){
        return notas[i];
    }
    
    public double getHist(int i){
        return hist[i];
    }

    public double getM() {
        return m;
    }
    
    public void setHist(int i, double hist){
        this.hist[i] = hist;
    }
    
    public void setNotas(int i, double notas){
        this.notas[i] = notas;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public void setM(double m){
        this.m = m;
    }
}
