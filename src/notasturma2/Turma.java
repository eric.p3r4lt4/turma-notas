package notasturma2;

public class Turma {
    private int t=10;
    private Aluno[] alunos = new Aluno[t];
    private double[] pesos = new double[3];
    
    public Turma(){
        for(int i=0;i<t;i++){
            alunos[i] = new Aluno();
        }
    }
    
    public int getT(){
        return t;
    }
    
    public void setPeso(int i, double pesos){
        this.pesos[i] = pesos;
    }
    
    public void setAlunosNome(int i, String nome){
        this.alunos[i].setNome(nome);
    }
    
    public void setAlunosNotas(int c, int i, double notas){
        this.alunos[c].setNotas(i, notas);
    }
    
    public void setAlunosHist(int c, int i, double hist){
        this.alunos[c].setHist(i, hist);
    }
    
    public double getPeso(int i){
        return pesos[i];
    }
    
    public void setMAluno(int i,double m){
        this.alunos[i].setM(m);
    }
    
    public double getNotaAluno(int c, int i){
        return alunos[c].getNotas(i);
    }
    
    public String getNomeAluno(int i){
        return alunos[i].getNome();
    }

    public double getHistAluno(int c, int i){
        return alunos[c].getHist(i);
    }
    
    public double getMAluno(int i){
        return alunos[i].getM();
    }
}